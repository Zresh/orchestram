import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { Scene, Router } from 'react-native-router-flux';
import { createStore, applyMiddleware } from 'redux';
import { connect, Provider } from 'react-redux';
import thunk from 'redux-thunk';
import reducer from '../reducers';
import Downloads from './Downloads';
import Rooms from './Rooms';
import Search from './Search';
import Player from './Player';
import MiniPlayer from './MiniPlayer';
import Styles from '../styles';
import Icon from 'react-native-vector-icons/FontAwesome';
import Playlist from "./Playlist";
import SocketIOClient from 'socket.io-client';
import ntpClient from 'react-native-ntp-client';
import clockSync from 'react-native-clock-sync'

const TabIcon = (props) => <Icon size={24} name={props.name} color={props.selected? "black": "#c8c3c3"}/>;

const store = createStore(reducer, applyMiddleware(thunk));
const RouterWithRedux = connect()(Router);

export default class App extends Component {

    constructor(props) {
        super(props);

        ntpClient.getNetworkTime("pool.ntp.org", 123, function(err, date) {
            if(err) {
                console.error(err);
                return;
            }

            console.log("Current time : ");
            console.log(date); // Mon Jul 08 2013 21:31:31 GMT+0200 (Paris, Madrid (heure d’été))
        });

        var socket = SocketIOClient('ws://46.101.111.38:3000');

        var options = {}
        ;

        var clock = new clockSync(options);



        setInterval(function() {
            var syncTime = clock.getTime();
            console.log('SyncTime:' + syncTime);
        }, 1000);

        let room=11;

        socket.on('connect', function() {
            socket.emit('room', room);
        });

 function getPing() {
     socket.emit('new message');
     console.log("go time = ");
 };

 function sync() {
     socket.emit('sync');
 };

 function Play() {
     socket.emit('message');

     setTimeout(getPing, 1000);
     setInterval(function(){ getPing();}, 15000);
     setInterval(function(){ sync();}, 1000);
 };

 Play();

 socket.on('new message', function () {
     console.log("go time = ");
 });

 socket.on('sync', function () {
     console.log("go time = ");
 });
    }




  render() {
    return (
     <Provider store={store}>
      <View style={{flex: 1}}>
      <MiniPlayer />
       <RouterWithRedux>
         <Scene key="root">
          <Scene key="home" initial tabs={true}>
              <Scene key="search" component={Search} title="Search" duration={0} icon={TabIcon} animation="fade"/>
              <Scene key="download" component={Downloads} initial title="Downloads" icon={TabIcon} duration={0} animation="fade"/>
              <Scene key="room" component={Rooms} title="Rooms" icon={TabIcon} animation="fade"/>
              <Scene key="playlist" component={Playlist} title="Playlist" icon={TabIcon} animation="fade"/>
          </Scene>
          <Scene key="player" component={Player} hideNavBar hideTabBar direction="vertical"/>
         </Scene>
       </RouterWithRedux>
       </View>
     </Provider>
   );
  }
}
